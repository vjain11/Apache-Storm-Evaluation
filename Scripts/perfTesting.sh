# Script to start Apache Storm on a 4 node cluster and run Income Category Topology with different Emit Rates

# Start Zookeeper Server on the master node
./zookeeper-3.4.6/bin/zkServer.sh start

# Start Nimbus on the master node
./apache-storm-1.0.2/bin/storm nimbus &

# Start UI on the master node
./apache-storm-1.0.2/bin/storm ui &

# Start Supervisors on the worker nodes
ssh vjain11@fa16-cs527-47.studentspace.cs.illinois.edu ./apache-storm-1.0.2/bin/storm supervisor &
ssh vjain11@fa16-cs527-48.studentspace.cs.illinois.edu ./apache-storm-1.0.2/bin/storm supervisor &
ssh vjain11@fa16-cs527-49.studentspace.cs.illinois.edu ./apache-storm-1.0.2/bin/storm supervisor &

# Wait for 2 minutes for Nimbus and all Supervisors to startup.
sleep 2m 

# Performance testing --> Income Categorization (At least once semantics)
# my-storm-perf-test contains the Topology class files. It can be downloaded from https://gitlab-beta.engr.illinois.edu/vjain11/my-storm-perf-test
# my-storm-perf-test is forked from Yahoo project --> https://github.com/yahoo/storm-perf-test
# Run each test for 5 min and note down the readings for latency and throughput from the Web UI.

# Emit Rate are varied using emit-count variable which is used inside Spout next tuple method
# For each experiment, vary the emit count to (1, 2, 4, 8, 16, 32, 64, 100) and rebuild the library.
# Once the library is rebuilt, run the following command
~/apache-storm-1.0.2/bin/storm jar my-storm-perf-test/target/storm_perf_test-1.0.0-SNAPSHOT-jar-with-dependencies.jar com.yahoo.storm.perftest.IncomeCategory --emitRate 1

# Repeat the above command for different emit-count settings and note down the latency and throughput readings thus obtained.
# Plot the Latency Vs Throughput graph for the 8 different readings obtained above
