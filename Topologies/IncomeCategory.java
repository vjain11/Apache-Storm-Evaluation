// Acknowledgement - This topology is a modified version of topologies at https://github.com/yahoo/storm-perf-test
// I have added new components/bolts which mimic the operations performed in real world topologies
package com.yahoo.storm.perftest;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.Utils;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.generated.Nimbus;
import org.apache.storm.generated.KillOptions;
import org.apache.storm.generated.ClusterSummary;
import org.apache.storm.generated.SupervisorSummary;
import org.apache.storm.generated.TopologySummary;
import org.apache.storm.generated.TopologyInfo;
import org.apache.storm.generated.ExecutorSummary;
import org.apache.storm.generated.ExecutorStats;
import org.apache.storm.generated.SpoutStats;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.task.TopologyContext;
import java.util.Map;
import java.util.Random;
import java.util.Arrays;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.base.BaseRichBolt;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;

public class IncomeCategory {
  private static final Logger LOG = LoggerFactory.getLogger(IncomeCategory.class);

  @Option(name="--help", aliases={"-h"}, usage="print help message")
  private boolean _help = false;
  
  @Option(name="--debug", aliases={"-d"}, usage="enable debug")
  private boolean _debug = false;
  
  @Option(name="--local", usage="run in local mode")
  private boolean _local = false;
  
  @Option(name="--messageSizeByte", aliases={"--messageSize"}, metaVar="SIZE",
      usage="size of the messages generated in bytes")
  private int _messageSize = 100;

  @Option(name="--numTopologies", aliases={"-n"}, metaVar="TOPOLOGIES",
      usage="number of topologies to run in parallel")
  private int _numTopologies = 1;
 
   @Option(name="--numLevels", aliases={"-l"}, metaVar="LEVELS",
      usage="number of levels of bolts per topolgy")
  private int _numLevels = 1;

  @Option(name="--spoutParallel", aliases={"--spout"}, metaVar="SPOUT",
      usage="number of spouts to run in parallel")
  private int _spoutParallel = 3;
  
  @Option(name="--boltParallel", aliases={"--bolt"}, metaVar="BOLT",
      usage="number of bolts to run in parallel")
  private int _boltParallel = 3;
  
  @Option(name="--numWorkers", aliases={"--workers"}, metaVar="WORKERS",
      usage="number of workers to use per topology")
  private int _numWorkers = 3;
  
  @Option(name="--ackers", metaVar="ACKERS", 
      usage="number of acker bolts to launch per topology")
  private int _ackers = 1;
  
  @Option(name="--maxSpoutPending", aliases={"--maxPending"}, metaVar="PENDING",
      usage="maximum number of pending messages per spout (only valid if acking is enabled)")
  private int _maxSpoutPending = -1;
  
  @Option(name="--name", aliases={"--topologyName"}, metaVar="NAME",
      usage="base name of the topology (numbers may be appended to the end)")
  private String _name = "test";
  
  @Option(name="--ackEnabled", aliases={"--ack"}, usage="enable acking")
  private boolean _ackEnabled = true;
  
  @Option(name="--pollFreqSec", aliases={"--pollFreq"}, metaVar="POLL",
      usage="How often should metrics be collected")
  private int _pollFreqSec = 30;
  
  @Option(name="--testTimeSec", aliases={"--testTime"}, metaVar="TIME",
      usage="How long should the benchmark run for.")
  private int _testRunTimeSec = 5 * 60;

  @Option(name="--db_path", aliases={"--db_path"}, metaVar="TIME",
      usage="Input file to read.")
  static private String db_name = "income_data.txt";
  
  @Option(name="--db_out_path", aliases={"--db_out_path"}, metaVar="TIME",
      usage="Output file to dump.")
  static private String db_out_name = "storm_output.txt";

public static class MyRandomSpout extends BaseRichSpout {
  SpoutOutputCollector _collector;
  Random _rand;
  private BufferedReader reader;
  private AtomicLong linesRead;
  private String fileName;
  private int count;

  @Override
  public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
    _collector = collector;
    _rand = new Random();
    count = 0;
/*
    linesRead = new AtomicLong(0);
    try {
      //fileName = (String) conf.get("/home/vjain11/income_data.txt");
      fileName = "/home/vjain11/income_data.txt";
      reader = new BufferedReader(new FileReader(fileName));
      // read and ignore the header if one exists
    } catch (Exception e) {
	    throw new RuntimeException(e);
    }
*/
  }

  @Override
  public void nextTuple() {
          if(count == 1) {
	     Utils.sleep(1);
             count = 0;
          } else {
             count++;
          }

	  String[] sentences = new String[]{ "Nathan:100", "Monika:400", "Rick:150", "Meghan:220", "Jina:1600", "Donna:1100", "Rachel:1400", "Blair:1700", "Sidd:2500", "Rohit:2700", "Vikas:2800", "Grace:3200", "Mike:3800", "Harvey:3900", "Allen:3560", "Tracy:0", "Frodo:0", "Soozy:0", "Mona:0" };
	  String sentence = sentences[_rand.nextInt(sentences.length)];
	  Object id = sentence;
	  _collector.emit(new Values(sentence), id);
   /*
	  try {
		  String line = reader.readLine();
		  if (line != null) {
		
			  long id = linesRead.incrementAndGet();
			  
			  _collector.emit(new Values(line), id);

		  } else {
			  System.out.println("Finished reading file, " + linesRead.get() + " lines read");
			  //Thread.sleep(10000);
		  }
	  } catch (Exception e) {
		  e.printStackTrace();
	  } 
  */
  }

  @Override
  public void ack(Object id) {
  }

  @Override
  public void fail(Object id) {
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields("word"));
  }

  @Override
  public void deactivate() {
    try {
      reader.close();
    } catch (IOException e) {
      LOG.warn("Problem closing file");
    }
  }
	
}

public static class FilterIncomes extends BaseRichBolt {
    OutputCollector _collector;
    public void prepare(Map conf, TopologyContext context,
            OutputCollector collector) {
        _collector = collector;
    }


    public void execute(Tuple tuple) {

        String sentence = tuple.getString(0);

        String [] arr = sentence.split(":");
        
	int income = Integer.parseInt(arr[1]);
 
	System.out.println(arr[0]);

	if (income > 0)
	{	
		_collector.emit(Utils.DEFAULT_STREAM_ID, Arrays.asList(tuple), new Values(sentence));
	}       
	_collector.ack(tuple);                                                      
    }                                                                               
    public void declareOutputFields(OutputFieldsDeclarer declarer) {                
        declarer.declare(new Fields("word"));                                       
    }                                                                               
  }                                                                                 
 

public static class AddTagsToUsers extends BaseRichBolt {
    OutputCollector _collector;
    public void prepare(Map conf, TopologyContext context,
            OutputCollector collector) {
        _collector = collector;
    }

    public void execute(Tuple tuple) {

        String sentence = tuple.getString(0);

        String [] arr = sentence.split(":");
        
	int income = Integer.parseInt(arr[1]);
 
	System.out.println(arr[0]);

	String tag;
	if (income < 1000)
	{	
		tag = "D";
	}       
	else if (income < 2000)
	{
		tag = "C";
	}
	else if (income < 3000)
	{
		tag = "B";
	}
	else
	{
		tag = "A";
	}
	
	_collector.emit(Utils.DEFAULT_STREAM_ID, Arrays.asList(tuple), new Values(arr[0], tag));
 
	_collector.ack(tuple);                                                      
    }                                                                               
    public void declareOutputFields(OutputFieldsDeclarer declarer) {                
        declarer.declare(new Fields("word", "tag"));                                       
    }                                                                               
  }                                                                                 

  public static class MyAggregatorBolt extends BaseRichBolt {

    OutputCollector collector;
    HashMap<String, ArrayList<String>> tagToUsersHash = new HashMap<String, ArrayList<String>>();                   

    HashMap<String, ArrayList<Tuple>> tagToTupleHash = new HashMap<String, ArrayList<Tuple>>(); 
    
    public void prepare(Map conf, TopologyContext context,
            OutputCollector _collector) {
        collector = _collector;
    }

    public void execute(Tuple tuple) {              
      
        String user = tuple.getString(0);                                             
        String tag = tuple.getString(1);
	ArrayList<String> users = new ArrayList<String>();
	ArrayList<Tuple> anchorTuples = new ArrayList<Tuple>();

	if(tagToUsersHash.containsKey(tag))
		users=tagToUsersHash.get(tag);

	if(tagToTupleHash.containsKey(tag))
                anchorTuples=tagToTupleHash.get(tag);

	users.add(user);
	anchorTuples.add(tuple);

	if(users.size() == 5)
	{
		String finalUsers = "";
		for(String currUser : users)
			finalUsers += (currUser + ":");
	
 		collector.emit(Utils.DEFAULT_STREAM_ID, anchorTuples, new Values(tag, finalUsers));
		for (Tuple t : anchorTuples)
		{
	              collector.ack(t);                                                      
		}
		
		anchorTuples.clear();
		users.clear();			
		tagToUsersHash.remove(tag);
		tagToTupleHash.remove(tag);
	} else {
		tagToUsersHash.put(tag,users);
        	tagToTupleHash.put(tag, anchorTuples);	
	}
      //collector.ack(tuple);
    }
	    
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
	    declarer.declare(new Fields("tag", "users"));
    }
  }

public static class MySinkBolt extends BaseRichBolt {
  private OutputCollector _collector;

  @Override
  public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
    _collector = collector;
  }

  @Override
  public void execute(Tuple tuple) {

	  try {
		  BufferedWriter output;
		  output = new BufferedWriter(new FileWriter("/home/vjain11/income_out.txt", true));
		  output.newLine();
		  output.append(tuple.getString(0) + ":" + tuple.getString(1));
		  output.close();
	  } catch (IOException e) {
	 
		  e.printStackTrace();
	  }
	  _collector.ack(tuple);
  }
  
  @Override
	  public void declareOutputFields(OutputFieldsDeclarer declarer) {
	//	  declarer.declare(new Fields("tag", "users"));
	  }

}

private static class MetricsState {
    long transferred = 0;
    int slotsUsed = 0;
    long lastTime = 0;
  }

  public void metrics(Nimbus.Client client, int size, int poll, int total) throws Exception {
    System.out.println("status\ttopologies\ttotalSlots\tslotsUsed\ttotalExecutors\texecutorsWithMetrics\ttime\ttime-diff ms\ttransferred\tthroughput (MB/s)\ttotal Failed");
    MetricsState state = new MetricsState();
    long pollMs = poll * 1000;
    long now = System.currentTimeMillis();
    state.lastTime = now;
    long startTime = now;
    long cycle = 0;
    long sleepTime;
    long wakeupTime;
    while (metrics(client, size, now, state, "WAITING")) {
      now = System.currentTimeMillis();
      cycle = (now - startTime)/pollMs;
      wakeupTime = startTime + (pollMs * (cycle + 1));
      sleepTime = wakeupTime - now;
      if (sleepTime > 0) {
        Thread.sleep(sleepTime);
      }
      now = System.currentTimeMillis();
    }

    now = System.currentTimeMillis();
    cycle = (now - startTime)/pollMs;
    wakeupTime = startTime + (pollMs * (cycle + 1));
    sleepTime = wakeupTime - now;
    if (sleepTime > 0) {
      Thread.sleep(sleepTime);
    }
    now = System.currentTimeMillis();
    long end = now + (total * 1000);
    do {
      metrics(client, size, now, state, "RUNNING");
      now = System.currentTimeMillis();
      cycle = (now - startTime)/pollMs;
      wakeupTime = startTime + (pollMs * (cycle + 1));
      sleepTime = wakeupTime - now;
      if (sleepTime > 0) {
        Thread.sleep(sleepTime);
      }
      now = System.currentTimeMillis();
    } while (now < end);
  }

  public boolean metrics(Nimbus.Client client, int size, long now, MetricsState state, String message) throws Exception {
    ClusterSummary summary = client.getClusterInfo();
    long time = now - state.lastTime;
    state.lastTime = now;
    int numSupervisors = summary.get_supervisors_size();
    int totalSlots = 0;
    int totalUsedSlots = 0;
    for (SupervisorSummary sup: summary.get_supervisors()) {
      totalSlots += sup.get_num_workers();
      totalUsedSlots += sup.get_num_used_workers();
    }
    int slotsUsedDiff = totalUsedSlots - state.slotsUsed;
    state.slotsUsed = totalUsedSlots;

    int numTopologies = summary.get_topologies_size();
    long totalTransferred = 0;
    int totalExecutors = 0;
    int executorsWithMetrics = 0;
    int totalFailed = 0;
    for (TopologySummary ts: summary.get_topologies()) {
      String id = ts.get_id();
      TopologyInfo info = client.getTopologyInfo(id);
      for (ExecutorSummary es: info.get_executors()) {
        ExecutorStats stats = es.get_stats();
        totalExecutors++;
        if (stats != null) {
          if (stats.get_specific().is_set_spout()) {
            SpoutStats ss = stats.get_specific().get_spout();
            Map<String, Long> failedMap = ss.get_failed().get(":all-time");
            if (failedMap != null) {
              for (String key: failedMap.keySet()) {
                Long tmp = failedMap.get(key);
                if (tmp != null) {
                  totalFailed += tmp;
                }
              }
            }
          }

          Map<String,Map<String,Long>> transferred = stats.get_transferred();
          if ( transferred != null) {
            Map<String, Long> e2 = transferred.get(":all-time");
            if (e2 != null) {
              executorsWithMetrics++;
              //The SOL messages are always on the default stream, so just count those
              Long dflt = e2.get("default");
              if (dflt != null) {
                totalTransferred += dflt;
              }
            }
          }
        }
      }
    }
    long transferredDiff = totalTransferred - state.transferred;
    state.transferred = totalTransferred;
    double throughput = (transferredDiff == 0 || time == 0) ? 0.0 : (transferredDiff * size)/(1024.0 * 1024.0)/(time/1000.0);
    System.out.println(message+"\t"+numTopologies+"\t"+totalSlots+"\t"+totalUsedSlots+"\t"+totalExecutors+"\t"+executorsWithMetrics+"\t"+now+"\t"+time+"\t"+transferredDiff+"\t"+throughput+"\t"+totalFailed);
    if ("WAITING".equals(message)) {
      //System.err.println(" !("+totalUsedSlots+" > 0 && "+slotsUsedDiff+" == 0 && "+totalExecutors+" > 0 && "+executorsWithMetrics+" >= "+totalExecutors+")");
    }
    return !(totalUsedSlots > 0 && slotsUsedDiff == 0 && totalExecutors > 0 && executorsWithMetrics >= totalExecutors);
  } 

 
  public void realMain(String[] args) throws Exception {
    Map clusterConf = Utils.readStormConfig();
    clusterConf.putAll(Utils.readCommandLineOpts());
    Nimbus.Client client = NimbusClient.getConfiguredClient(clusterConf).getClient();

    CmdLineParser parser = new CmdLineParser(this);
    parser.setUsageWidth(80);
    try {
      // parse the arguments.
      parser.parseArgument(args);
    } catch( CmdLineException e ) {
      // if there's a problem in the command line,
      // you'll get this exception. this will report
      // an error message.
      System.err.println(e.getMessage());
      _help = true;
    }
    if(_help) {
      parser.printUsage(System.err);
      System.err.println();
      return;
    }
    if (_numWorkers <= 0) {
      throw new IllegalArgumentException("Need at least one worker");
    }
    if (_name == null || _name.isEmpty()) {
      throw new IllegalArgumentException("name must be something");
    }
    if (!_ackEnabled) {
      _ackers = 0;
    }

    try {
      for (int topoNum = 0; topoNum < _numTopologies; topoNum++) {
        TopologyBuilder builder = new TopologyBuilder();
        LOG.info("Adding in "+_spoutParallel+" spouts");
	//Message size and acking cannot be varied.
        //builder.setSpout("messageSpout", 
        //    new SOLSpout(_messageSize, _ackEnabled), _spoutParallel);
	builder.setSpout("spout", 
	      new MyRandomSpout(), _spoutParallel);

        LOG.info("Adding in "+_boltParallel+" bolts");

	builder.setBolt("filter", new FilterIncomes(), _boltParallel).shuffleGrouping("spout");
	
	builder.setBolt("tags", new AddTagsToUsers(), _boltParallel).shuffleGrouping("filter");

	builder.setBolt("aggregator", new MyAggregatorBolt(), _boltParallel).fieldsGrouping("tags", new Fields("tag"));
	
	builder.setBolt("mysink", new MySinkBolt(), _boltParallel).shuffleGrouping("aggregator");

        //builder.setBolt("messageBolt1", new SOLBolt(), _boltParallel)
        //    .shuffleGrouping("messageSpout");
        //for (int levelNum = 2; levelNum <= _numLevels; levelNum++) {
        //  LOG.info("Adding in "+_boltParallel+" bolts at level "+levelNum);
        //  builder.setBolt("messageBolt"+levelNum, new SOLBolt(), _boltParallel)
        //      .shuffleGrouping("messageBolt"+(levelNum - 1));
        //}

        Config conf = new Config();
        conf.setDebug(_debug);
        conf.setNumWorkers(_numWorkers);
        conf.setNumAckers(_ackers);
        if (_maxSpoutPending > 0) {
          conf.setMaxSpoutPending(_maxSpoutPending);
        }

        StormSubmitter.submitTopology(_name+"_"+topoNum, conf, builder.createTopology());
      }
      metrics(client, _messageSize, _pollFreqSec, _testRunTimeSec);
    } finally {
      //Kill it right now!!!
      KillOptions killOpts = new KillOptions();
      killOpts.set_wait_secs(0);

      for (int topoNum = 0; topoNum < _numTopologies; topoNum++) {
        LOG.info("KILLING "+_name+"_"+topoNum);
        try {
          client.killTopologyWithOpts(_name+"_"+topoNum, killOpts);
        } catch (Exception e) {
          LOG.error("Error tying to kill "+_name+"_"+topoNum,e);
        }
      }
    }
  }
  
  public static void main(String[] args) throws Exception {
    new IncomeCategory().realMain(args);
  }
}
